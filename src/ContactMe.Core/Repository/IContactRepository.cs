﻿using System.Collections.Generic;
using ContactMe.Core.Domain;

namespace ContactMe.Core.Repository
{
    public interface IContactRepository
    {
        IEnumerable<Contact> GetAllContacts();
        void SaveContact(Contact c);
    }
}