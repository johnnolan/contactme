﻿using System.Web.Mvc;
using ContactMe.Core.Repository;

using StructureMap;

namespace ContactMe.Web.Infrastructure
{
    public class CustomControllerFactory : DefaultControllerFactory
    {
        private readonly Container container;

        public CustomControllerFactory()
        {
            this.container = new Container();
            AddBindings();
        }

        private void AddBindings()
        {
            this.container.Configure(x => x.For<IContactRepository>().Use<ContactRepository>());
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, System.Type controllerType)
        {
            return controllerType == null
                       ? null
                       : (IController)this.container.GetInstance(controllerType);
        }
    }
}